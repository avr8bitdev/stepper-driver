/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef HAL_DRIVERS_STEPPER_DRIVER_STEPPER_FUNCTIONS_H_
#define HAL_DRIVERS_STEPPER_DRIVER_STEPPER_FUNCTIONS_H_


#include "../../basic_includes/custom_types.h"
#include "../../MCAL_drivers/DIO_driver/DIO_functions.h"

// Stepper direction
typedef enum
{
    Stepper_direction_ClockWise,
    Stepper_direction_AntiClockWise
} Stepper_direction_t;

// DC stepper
typedef struct
{
    /*
               pin[1]         COM
                   *-///------/
                              |
       pin[2] *               |
              |               |
             ///             ///
              |               |
              |               * pin[0]
              /------///-*
                     pin[3]
    */

    DIO_port_t portStepper; // Stepper port
    u8 pinStepper[4];       // Stepper pins
    u8 Stepper_off_state;   // Stepper off state: ON, OFF
} Stepper_obj_t;

typedef const Stepper_obj_t* Stepper_cptr_t;

void Stepper_vidInit(Stepper_cptr_t structPtrStepperCpy);
void Stepper_vidRotate(Stepper_direction_t enumStepperDirectionCpy, u16 u16ItrCpy, Stepper_cptr_t structPtrStepperCpy);


#endif /* HAL_DRIVERS_STEPPER_DRIVER_STEPPER_FUNCTIONS_H_ */

