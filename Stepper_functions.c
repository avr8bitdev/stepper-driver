/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "Stepper_functions.h"
#include <util/delay.h>


// --- internal --- //
static void Stepper_vidRotate_ClockWise(u16 u16ItrCpy, Stepper_cptr_t structPtrStepperCpy)
{
    while (u16ItrCpy--)
    {
        for (s8 i = 3; i >= 0; i--) // activate each pin alone
        {
            DIO_vidSet_pinValue(structPtrStepperCpy->portStepper, structPtrStepperCpy->pinStepper[i], !(structPtrStepperCpy->Stepper_off_state));
            _delay_us(2400);
            DIO_vidSet_pinValue(structPtrStepperCpy->portStepper, structPtrStepperCpy->pinStepper[i], structPtrStepperCpy->Stepper_off_state);
        }
    }
}

static void Stepper_vidRotate_AntiClockWise(u16 u16ItrCpy, Stepper_cptr_t structPtrStepperCpy)
{
    while (u16ItrCpy--)
    {
        for (u8 i = 0; i < 4; i++) // activate each pin alone
        {
            DIO_vidSet_pinValue(structPtrStepperCpy->portStepper, structPtrStepperCpy->pinStepper[i], !(structPtrStepperCpy->Stepper_off_state));
            _delay_us(2400);
            DIO_vidSet_pinValue(structPtrStepperCpy->portStepper, structPtrStepperCpy->pinStepper[i], structPtrStepperCpy->Stepper_off_state);
        }
    }
}
// ---------------- //

void Stepper_vidInit(Stepper_cptr_t structPtrStepperCpy)
{
    for (u8 i = 0; i < 4; i++)
    {
        DIO_vidSet_pinDirection(structPtrStepperCpy->portStepper, structPtrStepperCpy->pinStepper[i], OUTPUT);
        DIO_vidSet_pinValue(structPtrStepperCpy->portStepper, structPtrStepperCpy->pinStepper[i], structPtrStepperCpy->Stepper_off_state);
    }
}

inline void Stepper_vidRotate(Stepper_direction_t enumStepperDirectionCpy, u16 u16ItrCpy, Stepper_cptr_t structPtrStepperCpy)
{
    if (enumStepperDirectionCpy == Stepper_direction_ClockWise)
        Stepper_vidRotate_ClockWise(u16ItrCpy, structPtrStepperCpy);
    else if (enumStepperDirectionCpy == Stepper_direction_AntiClockWise)
        Stepper_vidRotate_AntiClockWise(u16ItrCpy, structPtrStepperCpy);
}

